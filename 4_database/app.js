const Koa = require('koa')
const app = new Koa()
const Router = require('koa-router')
const router = new Router()
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')
const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    host:'localhost', 
    user: 'root', 
    database: 'exam'
});


app.use(serve('public'))

render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async (ctx, next) => {
    const [resultJoin] = await pool.query(`
        SELECT employee.id, firstname, lastname, job.job_name
        FROM employee
        INNER JOIN job ON employee.job_id = job.id
    `);
    const [resultJobNoEmployee] = await pool.query(`
        SELECT job.id, job.job_name
        FROM employee
        RIGHT JOIN job ON employee.job_id = job.id
        WHERE job_id IS NULL
    `);

    await ctx.render('index', {
        resultJoin: resultJoin,
        resultJobNoEmployee: resultJobNoEmployee
    });
})

app.use(router.routes())
app.listen(3000)